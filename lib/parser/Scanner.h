#ifndef SCANNER_H
#define SCANNER_H

#include <istream>
#include <string>
#include <list>

#include "Token.h"
#include "ProxyBuffer.h"

namespace parser {

const unsigned MAX_IDENT_LENGTH = 30;

class Scanner
{
public:
    Scanner(ProxyBuffer& buffer);

    Scanner(const Scanner&) = delete;
    const Scanner& operator=(const Scanner&) = delete;

    void readNextToken();
    Token getToken();

private:

    bool tryOperator();
    bool tryEof();
    bool tryValue();
    bool tryIdentifier();

    std::string readString();
    int readInt();

    char getNextChar();
    std::string resetBuffer();

    void ignoreWhitespaces();

    ProxyBuffer& buffer_;
    Token token_;
    InputPosition position_;
};

std::list<Token> readAllTokens(Scanner& scanner);

}

#endif // SCANNER_H

