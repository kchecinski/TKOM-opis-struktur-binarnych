#include "Token.h"

#include <map>

using namespace parser;

Token::Token()
    :type_(Type::Eof)
{

}

Token::Token(int v)
    :type_(Type::Int_val)
{
    value_.int_val = v;
}

Token::Token(bool v)
    :type_(Type::Bool_val)
{
    value_.bool_val = v;
}

Token::Token(const char *v)
    :type_(Type::Ident)
{
    id_val_ = v;
}

Token::Token(std::string v)
    :type_(Type::Ident)
{
    id_val_ = v;
}

Token::Token(ast::Main v)
    :type_(Type::Main)
{}

Token::Token(ast::Operator v)
{
    type_ = operatorToType(v);
    value_.operator_val = v;
}

Token::Token(ast::ValueType v)
{
    type_ = valueTypeToType(v);
    value_.type_val = v;
}

Token::Type Token::getType() const
{
    return type_;
}

int Token::getIntVal() const
{
    return value_.int_val;
}

bool Token::getBoolVal() const
{
    return value_.bool_val;
}

std::string Token::getIdentifier() const
{
    return id_val_;
}

ast::Operator Token::getOperator() const
{
    return value_.operator_val;
}

ast::ValueType Token::getValueType() const
{
    return value_.type_val;
}

Token::Type Token::operatorToType(ast::Operator oper)
{
    static std::map<ast::Operator, Token::Type> types {
        {ast::Operator::Plus,            Type::Add_op},
        {ast::Operator::Minus,           Type::Add_op},
        {ast::Operator::Multiplication,  Type::Mul_op},
        {ast::Operator::Ceil_division,   Type::Mul_op},
        {ast::Operator::Floor_division,  Type::Mul_op},
        {ast::Operator::Modulo_division, Type::Mul_op},
        {ast::Operator::Greater,         Type::Rel_op},
        {ast::Operator::Greater_equal,   Type::Rel_op},
        {ast::Operator::Less,            Type::Rel_op},
        {ast::Operator::Less_equal,      Type::Rel_op},
        {ast::Operator::Not_equal,       Type::Rel_op},
        {ast::Operator::Equal,           Type::Rel_op},
        {ast::Operator::Disjunction,     Type::Disj_op},
        {ast::Operator::Conjunction,     Type::Conj_op},
        {ast::Operator::Negation,        Type::Logic_neg_op},
        {ast::Operator::Reference,       Type::Ref_op},
        {ast::Operator::Dot,             Type::Dot},
        {ast::Operator::Colon,           Type::Colon},
        {ast::Operator::Comma,           Type::Comma},
        {ast::Operator::Cond_mark,       Type::Cond_mark},
        {ast::Operator::Left_bracket,    Type::L_bracket},
        {ast::Operator::Right_bracket,   Type::R_bracket},
        {ast::Operator::Left_sq_bracket, Type::Sq_L_bracket},
        {ast::Operator::Right_sq_bracket,Type::Sq_R_bracket}
    };
    return types[oper];
}

Token::Type Token::valueTypeToType(ast::ValueType type)
{
    static std::map<ast::ValueType, Token::Type> types {
        {ast::ValueType::Int8, Token::Type::Type_int},
        {ast::ValueType::Int16, Token::Type::Type_int},
        {ast::ValueType::Int32, Token::Type::Type_int},
        {ast::ValueType::String, Token::Type::Type_string},
    };
    return types[type];
}

std::string Token::toString(Token::Type type)
{
    static std::map<Token::Type, std::string> map = {
        {Token::Type::Dot,              "dot"},
        {Token::Type::Colon,            "colon"},
        {Token::Type::Comma,            "comma"},
        {Token::Type::L_bracket,        "left_bracket"},
        {Token::Type::R_bracket,        "right_bracket"},
        {Token::Type::Cond_mark,        "condition_mark"},
        {Token::Type::Rel_op,           "relational_operator"},
        {Token::Type::Disj_op,          "disjunction_operator"},
        {Token::Type::Conj_op,          "conjunction_operator"},
        {Token::Type::Add_op,           "additional_operator"},
        {Token::Type::Mul_op,           "multiplicative_operator"},
        {Token::Type::Logic_neg_op,     "logical_negation_operator"},
        {Token::Type::Ref_op,           "referention_operator"},
        {Token::Type::Int_val,          "integer"},
        {Token::Type::Bool_val,         "bool"},
        {Token::Type::Ident,            "identifier"},
        {Token::Type::Type_int,         "int_type"},
        {Token::Type::Type_string,      "string_type"},
        {Token::Type::Eof,              "end_of_file"},
        {Token::Type::Main,             "main"}
    };
    return map[type];
}

std::string Token::toString()
{
    return toString(type_);
}

void Token::setPosition(InputPosition pos)
{
    position_ = pos;
}

InputPosition Token::getPosition() const
{
    return position_;
}

