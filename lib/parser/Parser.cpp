#include "Parser.h"

using namespace parser;
using namespace ast;

Parser::Parser(Scanner &scanner)
    :scanner_(scanner)
{}

std::unique_ptr<File> Parser::parse()
{
    scanner_.readNextToken();
    std::unique_ptr<File> file;
    try {
        file = readFile();
    } catch (std::runtime_error e) {
        std::string what = e.what();
        throw std::runtime_error(what + " at " + curr_token_.getPosition().getUFPos().toString());
    }
    return std::move(file);
}

std::unique_ptr<File> Parser::readFile()
{
    setContext(std::shared_ptr<SymbolTable>(new SymbolTable));

    auto structures = readStructureList();
    auto main_structure = readMainStructure();

    std::unique_ptr<File> file = std::make_unique<File>(std::move(structures),
                                                        std::move(main_structure),
                                                        current_context_);
    return std::move(file);
}

std::unique_ptr<StructureList> Parser::readStructureList()
{
    std::unique_ptr<StructureList> structures (new StructureList);
    while (checkTokenType(Token::Type::Ident)) {
        auto structure = readStructure();
        structures->addStructure(std::move(structure));
    }
    return std::move(structures);
}

std::unique_ptr<Structure> Parser::readStructure()
{
    std::string ident = readIdentifier();
    setContext(std::shared_ptr<SymbolTable>(new SymbolTable));
    //TODO sprawdzic czy colon i ew. wczytać listę parametrów
    readOperator(Token::Type::Colon);
    auto field_list = readFieldList();

    std::unique_ptr<Structure> structure = std::make_unique<Structure>(ident,
                                                                       std::move(field_list),
                                                                       current_context_);
    backContext();
    return std::move(structure);
}

std::unique_ptr<Structure> Parser::readMainStructure()
{
    readMain();
    std::string ident = "Main";
    setContext(std::shared_ptr<SymbolTable>(new SymbolTable));
    readOperator(Token::Type::Colon);
    auto field_list = readFieldList();

    std::unique_ptr<Structure> structure = std::make_unique<Structure>(ident,
                                                                       std::move(field_list),
                                                                       current_context_);
    backContext();
    return std::move(structure);
}

std::unique_ptr<FieldList> Parser::readFieldList()
{
    std::unique_ptr<FieldList> field_list (new FieldList);
    auto field = readField();
    field_list->addField(std::move(field));
    while (checkTokenType(Token::Type::Comma)) {
        readOperator(Token::Type::Comma);;
        field = readField();
        field_list->addField(std::move(field));
    }
    readOperator(Token::Type::Dot);
    return std::move(field_list);
}

std::unique_ptr<Field> Parser::readField()
{
    if (checkTokenType(Token::Type::Type_int)) {
        return readIntField();
    } else if (checkTokenType(Token::Type::L_bracket)) {
        return readConditionalField();
    } else if (checkTokenType(Token::Type::Sq_L_bracket)) {
        return readSequence();
    }
    throwOnUnexpectedInput(Token::Type::Type_int);
}

std::unique_ptr<Field> Parser::readIntField()
{
    if(!current_context_) {
        throw std::runtime_error("Uninitialized context");
    }
    ast::ValueType type = readIntFieldType();
    std::string identifier = readIdentifier();
    return std::make_unique<IntField>(identifier,
                                      type,
                                      current_context_->registerInt(identifier, 0));
}

std::unique_ptr<Field> Parser::readConditionalField()
{
    setContext(std::shared_ptr<SymbolTable>(new SymbolTable(current_context_)));
    readOperator(Token::Type::L_bracket);
    auto condition = readLogicExpression();
    readOperator(Token::Type::R_bracket);
    readOperator(Token::Type::Cond_mark);
    readOperator(Token::Type::Colon);
    auto field_list = readFieldList();
    std::unique_ptr<ConditionalField> cond_field = std::make_unique<ConditionalField>(std::move(condition),
                                                                                      std::move(field_list),
                                                                                      current_context_);
    backContext();
    return std::move(cond_field);
}

std::unique_ptr<Field> Parser::readSequence()
{
    setContext(std::shared_ptr<SymbolTable>(new SymbolTable(current_context_)));
    readOperator(Token::Type::Sq_L_bracket);
    auto length = readArithmExpression();
    readOperator(Token::Type::Sq_R_bracket);
    std::string ident = readIdentifier();
    readOperator(Token::Type::Colon);
    auto fields = readFieldList();

    std::unique_ptr<Sequence> sequence = std::make_unique<Sequence>(std::move(length),
                                                                    ident,
                                                                    std::move(fields),
                                                                    current_context_);
    backContext();
    return std::move(sequence);

}

std::unique_ptr<LogicExpression> Parser::readLogicExpression()
{
    auto expr = readAlternative();

    while (checkTokenType(Token::Type::Disj_op)) {
        expr = readLogicExpression(std::move(expr));
    }
    return expr;
}

std::unique_ptr<LogicExpression> Parser::readAlternative()
{
    auto expr = readConjunct();

    while(checkTokenType(Token::Type::Conj_op)) {
        expr = readAlternative(std::move(expr));
    }
    return expr;
}

std::unique_ptr<LogicExpression> Parser::readConjunct()
{
    std::unique_ptr<LogicExpression> expr;
    if (checkTokenType(Token::Type::L_bracket)) {
        readOperator(Token::Type::L_bracket);
        expr = readLogicExpression();
        readOperator(Token::Type::R_bracket);
    } else if (checkTokenType(Token::Type::Logic_neg_op)) {
        expr = readNegateConjunct();
    } else if (checkTokenType(Token::Type::Bool_val)){
        expr = readLogicLiteral();
    } else {
        expr = readRelationalExpression();
    }
    return expr;
}

std::unique_ptr<LogicExpression> Parser::readLogicLiteral()
{
    const bool value = readBoolValue();
    return std::make_unique<BoolValue>(value);
}

std::unique_ptr<LogicExpression> Parser::readRelationalExpression()
{
    auto left_oper = readArithmExpression();
    Operator oper = readOperator(Token::Type::Rel_op);;
    auto right_oper = readArithmExpression();
    return std::make_unique<RelationalExpression>(std::move(left_oper),
                                                  oper,
                                                  std::move(right_oper));
}

std::unique_ptr<ArithmExpression> Parser::readArithmExpression()
{
    auto expr = readTerm();

    while (checkTokenType(Token::Type::Add_op) ) {
        expr = readArithmExpression(std::move(expr));
    }
    return expr;
}

std::unique_ptr<ArithmExpression> Parser::readArithmExpression(std::unique_ptr<ArithmExpression> left_oper)
{
    Operator oper = readOperator(Token::Type::Add_op);;
    std::unique_ptr<ArithmExpression> right_oper;
    if (checkTokenType(Token::Type::L_bracket)) {
        readOperator(Token::Type::L_bracket);
        right_oper = readArithmExpression();
        readOperator(Token::Type::R_bracket);
    } else {
        right_oper = readTerm();
    }
    return std::make_unique<ArithmBinaryExpression>(std::move(left_oper),
                                              oper,
                                              std::move(right_oper));

}

std::unique_ptr<ArithmExpression> Parser::readTerm()
{
    auto expr = readFactor();

    while (checkTokenType(Token::Type::Mul_op)) {
        expr = readTerm(std::move(expr));
    }
    return expr;
}

std::unique_ptr<ArithmExpression> Parser::readTerm(std::unique_ptr<ArithmExpression> left_oper)
{
    Operator oper = readOperator(Token::Type::Mul_op);
    std::unique_ptr<ArithmExpression> right_oper;
    if (checkTokenType(Token::Type::L_bracket)) {
        readOperator(Token::Type::L_bracket);
        right_oper = readArithmExpression();
        readOperator(Token::Type::R_bracket);
    } else {
        right_oper = readFactor();
    }
    return std::make_unique<ArithmBinaryExpression>(std::move(left_oper),
                                              oper,
                                                    std::move(right_oper));
}

std::unique_ptr<ArithmExpression> Parser::readVariable()
{
    std::string ident = readIdentifier();
    if (checkTokenType(Token::Type::Ref_op)) {
        //TODO reference
    } else {
        return std::make_unique<IntVariable>(ident, current_context_);
    }
}

std::unique_ptr<ArithmExpression> Parser::readFactor()
{
    std::unique_ptr<ArithmExpression> value;
    if (checkTokenType(Token::Type::L_bracket)) {
        readOperator(Token::Type::L_bracket);
        value = readArithmExpression();
        readOperator(Token::Type::R_bracket);
    } else {
        value = readLiteral();
    }
    return std::move(value);


}

std::unique_ptr<ArithmExpression> Parser::readLiteral()
{
    if (checkTokenType(Token::Type::Ident)) {
        std::string ident = readIdentifier();
        return std::make_unique<IntVariable>(ident, current_context_);
    }
    const int value = readIntValue();
    return std::make_unique<IntValue>(value);
}

std::unique_ptr<LogicExpression> Parser::readLogicExpression(std::unique_ptr<LogicExpression> left_oper)
{
    Operator oper = readOperator(Token::Type::Disj_op);
    std::unique_ptr<LogicExpression> right_oper;
    if (checkTokenType(Token::Type::L_bracket)) {
        readOperator(Token::Type::L_bracket);
        right_oper = readLogicExpression();
        readOperator(Token::Type::R_bracket);
    } else {
        right_oper = readAlternative();
    }
    return std::make_unique<LogicBinaryExpression>(std::move(left_oper),
                                                   oper,
                                                   std::move(right_oper));
}

std::unique_ptr<LogicExpression> Parser::readAlternative(std::unique_ptr<LogicExpression> left_oper)
{
    Operator oper = readOperator(Token::Type::Conj_op);
    std::unique_ptr<LogicExpression> right_oper;
    if (checkTokenType(Token::Type::L_bracket)) {
        readOperator(Token::Type::L_bracket);
        right_oper = readLogicExpression();
        readOperator(Token::Type::R_bracket);
    } else {
        right_oper = readConjunct();
    }
    return std::make_unique<LogicBinaryExpression>(std::move(left_oper),
                                                   oper,
                                                   std::move(right_oper));
}

std::unique_ptr<LogicExpression> Parser::readNegateConjunct()
{
    Operator oper = readOperator(Token::Type::Logic_neg_op);
    std::unique_ptr<LogicExpression> expr;
    if (checkTokenType(Token::Type::L_bracket)) {
        readOperator(Token::Type::L_bracket);
        expr = readLogicExpression();
        readOperator(Token::Type::R_bracket);
    } else if (checkTokenType(Token::Type::Bool_val)) {
        expr = readLogicLiteral();
    } else {
        expr = readRelationalExpression();
    }
    return std::make_unique<LogicUnaryExpression>(oper,
                                                  std::move(expr));
}


int Parser::readIntValue()
{
    const auto ret = requireToken(Token::Type::Int_val).getIntVal();
    advance();
    return ret;
}

bool Parser::readBoolValue()
{
    const auto ret = requireToken(Token::Type::Bool_val).getBoolVal();
    advance();
    return ret;
}

bool Parser::checkTokenType(Token::Type type)
{
    return scanner_.getToken().getType() == type;
}

std::string Parser::readIdentifier()
{
    std::string ret = requireToken(Token::Type::Ident).getIdentifier();
    advance();
    return ret;
}

Operator Parser::readOperator(Token::Type op)
{
    const auto ret = requireToken(op).getOperator();
    advance();
    return ret;
}

//Keyword Parser::readMain()
//{
//    const auto ret = requireToken(Token::Type::Main).getKeyword();
//    advance();
//    return ret;
//}

ValueType Parser::readIntFieldType()
{
    const auto ret = requireToken(Token::Type::Type_int).getValueType();
    advance();
    return ret;
}

ValueType Parser::readStringFieldType()
{
    const auto ret = requireToken(Token::Type::Type_string).getValueType();
    advance();
    return ret;
}

void Parser::readMain()
{
    requireToken(Token::Type::Main);
    advance();
}


Token Parser::requireToken(Token::Type expected)
{
    const auto token = scanner_.getToken();
    const auto type = token.getType();
    if (type != expected)
        throwOnUnexpectedInput(expected);
    return token;
}

void Parser::advance()
{
    curr_token_ = scanner_.getToken();
    scanner_.readNextToken();
}

void Parser::throwOnUnexpectedInput(Token::Type expected)
{
    throw std::runtime_error("Unexpected token: " + scanner_.getToken().toString() +
                             ", expecting: " + Token::toString(expected));
}

void Parser::setContext(std::shared_ptr<SymbolTable> context)
{
    current_context_ = context;
}

void Parser::backContext()
{
    current_context_ = current_context_->getParent();
}

