#include "Scanner.h"

#include "AnalysisErrors.h"

#include <map>
#include <limits.h>

using namespace ast;
using namespace parser;

Scanner::Scanner(ProxyBuffer &buffer)
    :buffer_(buffer)
{}

void Scanner::readNextToken()
{
    position_ = buffer_.getInputPos();
    if(tryEof()||tryOperator()||tryValue()||tryIdentifier()){
        buffer_.ignoreWhitespaces();
        token_.setPosition(position_);
        return;
    }
    else
        throw ScannerException(ScannerException::Type::TOKEN_UNRECOGNISED, position_);
}

Token Scanner::getToken()
{
    return token_;
}

bool Scanner::tryOperator()
{
    static std::map<char, Operator> single_op {
        {'.', Operator::Dot},
        {':', Operator::Colon},
        {',', Operator::Comma},
        {'(', Operator::Left_bracket},
        {')', Operator::Right_bracket},
        {'?', Operator::Cond_mark},
        {'*', Operator::Multiplication},
        {'+', Operator::Plus},
        {'%', Operator::Modulo_division},
        {'[', Operator::Left_sq_bracket},
        {']', Operator::Right_sq_bracket}

    };

    if (isalnum(buffer_.peek()))
    {
        return false;
    }
    char ch = buffer_.get();
    auto it = single_op.find(ch);

    if ( it != single_op.end() ) {
        token_ = Token(it->second);
        return true;
    }

    if ( ch == '!' ) {
        if (buffer_.peek() == '=' ) {
            token_ = Token(Operator::Not_equal);
            buffer_.get();

            return true;
        } else {
            token_ = Token(Operator::Negation);

            return true;
        }
    }
    if ( ch == '-' ) {
        if (buffer_.peek() == '>' ) {
            token_ = Token(Operator::Reference);
            buffer_.get();
        } else {
            token_ = Token(Operator::Minus);
        }

        return true;
    }
    if ( ch == '|' ) {
        if (buffer_.peek() == '|') {
            token_ = Token(Operator::Disjunction);
            buffer_.get();
            return true;
        }

        return false;
    }
    if ( ch == '&' ) {
        if (buffer_.peek() == '&') {
            token_ = Token(Operator::Conjunction);
            buffer_.get();
            return true;
        }

        return false;
    }
    if ( ch == '=' ) {
        if (buffer_.peek() == '=') {
            token_ = Token(Operator::Equal);
            buffer_.get();
            return true;
        }

        return false;
    }
    if ( ch == '>' ) {
        if (buffer_.peek() == '=' ) {
            token_ = Token(Operator::Greater_equal);
            buffer_.get();
        } else {
            token_ = Token(Operator::Greater);
        }

        return true;
    }
    if ( ch == '<' ) {
        if (buffer_.peek() == '=' ) {
            token_ = Token(Operator::Less_equal);
            buffer_.get();
        } else
            token_ = Token(Operator::Less);

        return true;
    }
    if ( ch == '/' ) {
        if (buffer_.peek() == '+') {
            token_ = Token(Operator::Ceil_division);
            buffer_.get();

            return true;
        } else if (buffer_.peek() == '-') {
            token_ = Token(Operator::Floor_division);
            buffer_.get();

            return true;
        }
        else
        return false;
        // Bledny token
    }

    return false;
}

bool Scanner::tryEof()
{
    if ( buffer_.eof() ) {
        token_ = Token();
        return true;
    }
    return false;
}

bool Scanner::tryValue()
{
    int val = readInt();
    if (val == -1) {

        return false;
    }
    token_ = Token(val);

    return true;
}

bool Scanner::tryIdentifier()
{
    std::string str = readString();
    if (str.empty()) {
        return false;
    }
    if (str == "True") {
        token_ = Token(true);
        return true;
    }
    if (str == "False") {
        token_ = Token(false);
        return true;
    }
    if (str == "Main") {
        token_ = Token(ast::Main::Main);
        return true;
    }

    static std::map< std::string, ast::ValueType > valueTypesMap = {
        {"int8", ValueType::Int8},
        {"int16", ValueType::Int16},
        {"int32", ValueType::Int32},
        {"str", ValueType::String}
    };

    if (valueTypesMap.find(str) != valueTypesMap.end()) {
        token_ = Token(valueTypesMap[str]);
        return true;
    }

    token_ = Token(str);

    return true;
}

std::string Scanner::readString()
{
    std::string str;
    if (isalpha(buffer_.peek())) {
        str += buffer_.get();
    } else {
        return str;
    }
    while (isalnum(buffer_.peek())){
        str += buffer_.get();
        if (str.size() > MAX_IDENT_LENGTH) {
            throw ScannerException(ScannerException::Type::IDENTIFIER_TOO_LONG, position_);
        }
    }
    return str;
}

int Scanner::readInt()
{
    unsigned long val = 0;
    if (!isdigit(buffer_.peek())) {
        return -1;
    }
    while (isdigit(buffer_.peek())) {
        val *= 10;
        val += (int)buffer_.get() - 48;
        if (val > UINT_MAX) {
            throw ScannerException(ScannerException::Type::INT_OVERFLOW, position_);
        }
    }
    return val;
}

std::list<Token> parser::readAllTokens(Scanner &scanner)
{
    std::list<Token> tokens;
    do {
        scanner.readNextToken();
        tokens.push_back(scanner.getToken());
    } while (tokens.back().getType() != Token::Type::Eof);
    return tokens;
}
