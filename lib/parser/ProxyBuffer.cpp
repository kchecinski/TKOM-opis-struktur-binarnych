#include "ProxyBuffer.h"

#include <ctype.h>

using namespace parser;

ProxyBuffer::ProxyBuffer(std::istream& in)
    :in_(in)
{
    ignoreWhitespaces();
}

char ProxyBuffer::get()
{
    char next = in_.get();
    updateInputPos(next);
    return next;
}

char ProxyBuffer::peek()
{
    return in_.peek();
}

void ProxyBuffer::ignoreWhitespaces()
{
    while (in_ && std::isspace(in_.peek())) {
        updateInputPos(in_.get());
    }
}

bool ProxyBuffer::eof()
{
    return in_.eof() || in_.peek() == -1;
}

InputPosition ProxyBuffer::getInputPos() const
{
    return input_pos_;
}

void ProxyBuffer::updateInputPos(char ch)
{
    input_pos_.nextChar();
    if (ch == '\n')
        input_pos_.nextLine();
}

