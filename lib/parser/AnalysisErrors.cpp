#include "AnalysisErrors.h"

using namespace parser;

ScannerException::ScannerException(Type type, InputPosition pos)
    :type_(type), position_(pos) { }

std::string ScannerException::what() const
{
    switch (type_) {
    case Type::INT_OVERFLOW:
        return "Too big integer value at " + position_.getUFPos().toString();
    case Type::IDENTIFIER_TOO_LONG:
        return "Too long identifier at " + position_.getUFPos().toString();
    case Type::TOKEN_UNRECOGNISED:
        return "Unrecognised token at " + position_.getUFPos().toString();
    default:
        return "Unknown exception type at " + position_.getUFPos().toString();
    }
}
