#ifndef ARITHMEXPRESSION_H
#define ARITHMEXPRESSION_H

#include <string>

namespace ast{

class ArithmExpression
{
public:
    virtual ~ArithmExpression();

    virtual int calculate() const = 0;
    virtual std::string toString() const = 0;
};

}
#endif // ARITHMEXPRESSION_H
