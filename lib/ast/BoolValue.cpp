#include "BoolValue.h"

using namespace ast;

BoolValue::BoolValue(bool value)
    :value_(value)
{}

bool BoolValue::calculate() const
{
    return value_;
}

std::string BoolValue::toString() const
{
    return value_ ? "True" : "False";
}
