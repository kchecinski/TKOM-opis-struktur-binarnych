#include "LogicBinaryExpression.h"

using namespace ast;

LogicBinaryExpression::LogicBinaryExpression(std::unique_ptr<LogicExpression> left_operand,
                                             Operator op,
                                             std::unique_ptr<LogicExpression> right_operand)
    :left_operand_(std::move(left_operand))
    ,operator_(op)
    ,right_operand_(std::move(right_operand))
{}

bool LogicBinaryExpression::calculate() const
{
    bool left_value = left_operand_->calculate();
    bool right_value = right_operand_->calculate();

    return calculate(left_value, right_value);
}

std::string LogicBinaryExpression::toString() const
{
    return left_operand_->toString() + ast::toString(operator_) + right_operand_->toString();
}

bool LogicBinaryExpression::calculate(bool left_value, bool right_value) const
{
//    static std::map<Operator, std::function<bool(bool,bool)> > operations {
//        {Operator::Disjunction,        [](bool a, bool b){return a || b;}},
//        {Operator::Conjunction,        [](bool a, bool b){return a && b;}}
//    }

    switch (operator_) {
    case Operator::Conjunction:
        return left_value && right_value;
    case Operator::Disjunction:
        return left_value || right_value;
    default:
        throw std::runtime_error("Wrong operator type. Logical operator expected. Given: " + ast::toString(operator_));
    }

}

