#ifndef FIELDTYPE_H
#define FIELDTYPE_H

#include <string>

namespace ast{

enum class ValueType
{
    Int8,
    Int16,
    Int32,
    String
};

enum class Main {
    Main
};

std::string toString(ValueType o);
inline std::ostream& operator<<(std::ostream& out, ValueType o) {return out << toString(o);}

}

#endif // FIELDTYPE_H
