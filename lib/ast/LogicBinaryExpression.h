#ifndef LOGICBINARYEXPRESSION_H
#define LOGICBINARYEXPRESSION_H

#include "LogicExpression.h"
#include "Operator.h"

#include <memory>

namespace ast {

class LogicBinaryExpression: public LogicExpression
{
public:
    LogicBinaryExpression(std::unique_ptr<LogicExpression> left_operand,
                          Operator op,
                          std::unique_ptr<LogicExpression> right_operand);
    bool calculate() const override;
    std::string toString() const override;

private:
    bool calculate(bool left_value, bool right_value) const;

    std::unique_ptr<LogicExpression> left_operand_;
    Operator operator_;
    std::unique_ptr<LogicExpression> right_operand_;
};

}

#endif // LOGICBINARYEXPRESSION_H
