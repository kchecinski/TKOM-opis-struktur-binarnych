#ifndef IDENTIFIER_H
#define IDENTIFIER_H

#include <string>

namespace ast
{

class Identifier
{
    enum Type {
        Unspecified = 0,
        Structure,
        Field,
        Parameter
    };

public:
    Identifier(Type type, std::string name);
    std::string getName() const;
    Type getType() const;
private:
    std::string name_;
    Type type_;
};

} // namespace ast

#endif // IDENTIFIER_H
