#ifndef SEQUENCE_H
#define SEQUENCE_H

#include "Field.h"
#include "ArithmExpression.h"
#include "FieldList.h"
#include "SymbolTable.h"

namespace ast {

class Sequence: public Field
{
public:
    Sequence(std::unique_ptr<ArithmExpression> length,
             std::string ident,
             std::unique_ptr<FieldList> fields,
             std::shared_ptr<SymbolTable> table);

    void fill(std::istream &infile) override;
    void read(std::ostream &outfile) override;

    std::string toString() const override;

private:
    std::unique_ptr<ArithmExpression> length_expr_;
    std::string ident_;
    std::unique_ptr<FieldList> fields_;
    std::shared_ptr<SymbolTable> table_;

};

}

#endif // SEQUENCE_H
