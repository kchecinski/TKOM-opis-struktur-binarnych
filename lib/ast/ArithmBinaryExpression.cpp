#include "ArithmBinaryExpression.h"

#include <functional>
#include <map>
#include <cmath>
#include <exception>

using namespace ast;

ArithmBinaryExpression::ArithmBinaryExpression(std::unique_ptr<ArithmExpression> left_operand,
                                   Operator op,
                                   std::unique_ptr<ArithmExpression> right_operand)
    :left_operand_(std::move(left_operand))
    ,operator_(op)
    ,right_operand_(std::move(right_operand))
{}

int ArithmBinaryExpression::calculate() const
{
    int left_val = left_operand_->calculate();
    int right_val = right_operand_->calculate();

    return calculate(left_val, right_val);
}

std::string ArithmBinaryExpression::toString() const
{
    return left_operand_->toString() + ast::toString(operator_) + right_operand_->toString();
}

int ArithmBinaryExpression::calculate(int left_value, int right_value) const
{
    static std::map<Operator, std::function<int(int,int)> > operations {
        {Operator::Plus,            [](int a,int b){return a + b;}},
        {Operator::Minus,           [](int a,int b){return a - b;}},
        {Operator::Multiplication,  [](int a,int b){return a * b;}},
        {Operator::Ceil_division,   [](int a,int b){return (int)ceil((float)a / b);}},
        {Operator::Floor_division,  [](int a,int b){return (int)floor((float)a / b);}},
        {Operator::Modulo_division, [](int a,int b){return a % b;}}
    };
    auto operation = operations.find(operator_);
    if (operation == operations.end()) {
        throw std::runtime_error("Wrong operator type. Given: " + ast::toString(operator_));
    }
    return operations[operator_](left_value, right_value);
}

