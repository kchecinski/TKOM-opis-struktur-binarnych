#ifndef INTVARIABLE_H
#define INTVARIABLE_H

#include <iostream>
#include <memory>

#include "ArithmExpression.h"
#include "SymbolTable.h"

namespace ast {

class IntVariable: public ArithmExpression
{
public:
    IntVariable(std::string ident, std::shared_ptr<SymbolTable> sym_table);

    int calculate() const override;
    std::string toString() const override;
private:
    std::string identifier_;
    const int& value_ref_;

};

}

#endif // INTVARIABLE_H
