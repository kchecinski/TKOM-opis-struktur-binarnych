#include "Identifier.h"

using namespace ast;

Identifier::Identifier(Type type, std::string name)
    :type_(type)
    ,name_(name)
{}
