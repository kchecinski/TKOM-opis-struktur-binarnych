#include "IntValue.h"

using namespace ast;

IntValue::IntValue(int value)
    :value_(value)
{}

int ast::IntValue::calculate() const
{
    return value_;
}

std::string IntValue::toString() const
{
    return std::to_string(value_);
}

