#ifndef STRUCTURELIST_H
#define STRUCTURELIST_H

#include "Structure.h"

namespace ast {

class StructureList
{
public:
    StructureList();

    std::string toString() const;
    void addStructure( std::unique_ptr<Structure> structure);

private:
    std::list< std::unique_ptr<Structure> > structures_;
};

}

#endif // STRUCTURELIST_H
