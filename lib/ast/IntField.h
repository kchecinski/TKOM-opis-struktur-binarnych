#ifndef INTFIELD_H
#define INTFIELD_H

#include "StandardField.h"

namespace ast {

class IntField : public StandardField
{
public:
    IntField(const std::string &ident, ValueType type, int &value_ref);

    void fill(std::istream &in) override;
    void read(std::ostream &out) override;

    std::string toString() const override;
private:
    int& value_ref_;
};


}
#endif // INTFIELD_H
