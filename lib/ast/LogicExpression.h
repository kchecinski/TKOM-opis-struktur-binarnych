#ifndef LOGICEXPRESSION_H
#define LOGICEXPRESSION_H

#include <string>

namespace ast {

class LogicExpression
{
public:
    virtual ~LogicExpression();

    virtual bool calculate() const = 0;
    virtual std::string toString() const = 0;
};

}

#endif // LOGICEXPRESSION_H
