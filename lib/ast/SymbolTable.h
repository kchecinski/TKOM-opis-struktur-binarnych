#ifndef SYMBOLTABLE_H
#define SYMBOLTABLE_H

#include <memory>
#include <string>
#include <map>

class SymbolTable
{
public:
    SymbolTable();
    SymbolTable(std::shared_ptr<SymbolTable> parent);

    int& registerInt(const std::string& ident, int default_val);
    const int& getIntRef(const std::string& ident) const;

    std::shared_ptr<SymbolTable> getParent() const;
private:

    std::shared_ptr<SymbolTable> parent_;
    std::map<std::string, int> int_values_;
};

#endif // SYMBOLTABLE_H
