#ifndef LOGICUNARYEXPRESSION_H
#define LOGICUNARYEXPRESSION_H

#include "LogicExpression.h"
#include "Operator.h"

#include <memory>

namespace ast {

class LogicUnaryExpression: public LogicExpression
{
public:
    LogicUnaryExpression(Operator op,
                         std::unique_ptr<LogicExpression> operand);
    bool calculate() const override;
    std::string toString() const override;

private:
    bool calculate(bool value) const;

    Operator operator_;
    std::unique_ptr<LogicExpression> operand_;

};

}

#endif // LOGICUNARYEXPRESSION_H
