#include "ConditionalField.h"

using namespace ast;

ConditionalField::ConditionalField(std::unique_ptr<LogicExpression> condition,
                                        std::unique_ptr<FieldList> fields,
                                        std::shared_ptr<SymbolTable> table)
    :condition_(std::move(condition))
    ,fields_(std::move(fields))
    ,table_(table)
{}

void ConditionalField::fill(std::istream &in)
{
    if (condition_->calculate())
        fields_->fill(in);
}

void ConditionalField::read(std::ostream &outfile)
{
    if (condition_->calculate())
        fields_->read(outfile);
}

std::string ConditionalField::toString() const
{
    return "(" + condition_->toString() + ")?: " + fields_->toString();
}
