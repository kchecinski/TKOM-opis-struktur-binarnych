#include "SymbolTable.h"

SymbolTable::SymbolTable()
{}

SymbolTable::SymbolTable(std::shared_ptr<SymbolTable> parent)
    :parent_(parent)
{}

int &SymbolTable::registerInt(const std::string &ident, int default_val)
{
    auto it = int_values_.find(ident);
    if (it != int_values_.end())
        throw std::runtime_error("Symbol " + ident + " already exists");
    int_values_[ident] = default_val;
    return int_values_[ident];

}

const int &SymbolTable::getIntRef(const std::string &ident) const
{
    auto it = int_values_.find(ident);
    if (it == int_values_.end()) {
        if (parent_ != nullptr) {
            return parent_->getIntRef(ident);
        }
        else {
            throw std::runtime_error("Undefined symbol " + ident);
        }
    }
    return int_values_.at(ident);
}

std::shared_ptr<SymbolTable> SymbolTable::getParent() const
{
    return parent_;
}
