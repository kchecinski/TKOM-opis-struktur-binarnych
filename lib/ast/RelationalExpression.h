#ifndef RELATIONALEXPRESSION_H
#define RELATIONALEXPRESSION_H

#include "LogicExpression.h"
#include "ArithmExpression.h"

#include "Operator.h"

#include <memory>
#include <map>
#include <functional>

namespace ast {

class RelationalExpression: public LogicExpression
{
public:
    RelationalExpression(std::unique_ptr<ArithmExpression> left_operand,
                         Operator op,
                         std::unique_ptr<ArithmExpression> right_operand);

    bool calculate() const override;
    std::string toString() const override;

private:
    bool calculate(int left_value, int right_value) const;

    std::unique_ptr<ArithmExpression> left_operand_;
    Operator operator_;
    std::unique_ptr<ArithmExpression> right_operand_;
};

}

#endif // RELATIONALEXPRESSION_H
