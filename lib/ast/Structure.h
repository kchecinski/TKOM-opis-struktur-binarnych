#ifndef STRUCTURE_H
#define STRUCTURE_H

#include <memory>
#include <list>

#include "SymbolTable.h"
#include "IntField.h"
#include "FieldList.h"

namespace ast {

class Structure
{
public:
    Structure(std::string identifier, std::unique_ptr<FieldList> fields, std::shared_ptr<SymbolTable> table);

    const std::string& getIdentifier() const;
    //const std::list<std::unique_ptr<Field> >& getFields() const;

    std::shared_ptr<SymbolTable> getSymbolTable() const;

    std::string toString() const;

    void fill(std::istream &in);
private:
    std::string identifier_;
    std::shared_ptr<SymbolTable> table_;
    std::unique_ptr<FieldList> fields_;
};

}

#endif // STRUCTURE_H
