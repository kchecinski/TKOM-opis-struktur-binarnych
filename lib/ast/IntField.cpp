#include "IntField.h"

#include <iostream>

using namespace ast;


IntField::IntField(const std::string &ident, ast::ValueType type, int& value_ref)
    :StandardField(ident, type)
    ,value_ref_(value_ref)
{}

void IntField::fill(std::istream &in)
{
    in>>value_ref_;
}

void IntField::read(std::ostream &out)
{
    out<<value_ref_;
}

std::string IntField::toString() const
{
    return ast::toString(type_) + " " + getIdentifier();
}
