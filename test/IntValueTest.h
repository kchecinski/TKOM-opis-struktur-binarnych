#ifndef ARITHMEXPRESSIONTESTS_H
#define ARITHMEXPRESSIONTESTS_H

#include <QObject>
#include <QtTest/QtTest>

#include "../lib/ast/IntValue.h"

class IntValueTest : public QObject
{
    Q_OBJECT
public:
    IntValueTest(){}

private slots:
    void returnsGivenValue();
    void printsProperlyString();
};

#endif // ARITHMEXPRESSIONTESTS_H
