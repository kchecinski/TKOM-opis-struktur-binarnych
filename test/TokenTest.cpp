#include "TokenTest.h"

using namespace parser;

TokenTest::TokenTest()
{
}

void TokenTest::init()
{
    Token token;
    QVERIFY(token.getType() == Token::Type::Eof);
}

void TokenTest::initInt()
{
    Token token(10);
    QVERIFY(token.getType() == Token::Type::Int_val);
    QVERIFY(token.getIntVal() == 10);
}

void TokenTest::initBool()
{
    Token token(true);
    QVERIFY(token.getType() == Token::Type::Bool_val);
    QVERIFY(token.getBoolVal() == true);
}

void TokenTest::initPlus() {
    Token token(ast::Operator::Plus);
    QVERIFY(token.getType() == Token::Type::Add_op);
    QVERIFY(token.getOperator() == ast::Operator::Plus);
}

void TokenTest::initModulo() {
    Token token2(ast::Operator::Modulo_division);
    QVERIFY(token2.getType() == Token::Type::Mul_op);
    QVERIFY(token2.getOperator() == ast::Operator::Modulo_division);
}

void TokenTest::initDisjunction() {
    Token token4(ast::Operator::Disjunction);
    QVERIFY(token4.getType() == Token::Type::Disj_op);
    QVERIFY(token4.getOperator() == ast::Operator::Disjunction);
}

void TokenTest::initConjunction() {
    Token token5(ast::Operator::Conjunction);
    QVERIFY(token5.getType() == Token::Type::Conj_op);
    QVERIFY(token5.getOperator() == ast::Operator::Conjunction);
}

void TokenTest::initGreaterOper() {
    Token token3(ast::Operator::Greater);
    QVERIFY(token3.getType() == Token::Type::Rel_op);
    QVERIFY(token3.getOperator() == ast::Operator::Greater);
}

void TokenTest::initNegOper() {
    Token token6(ast::Operator::Negation);
    QVERIFY(token6.getType() == Token::Type::Logic_neg_op);
    QVERIFY(token6.getOperator() == ast::Operator::Negation);
}

void TokenTest::initRefOper() {
    Token token7(ast::Operator::Reference);
    QVERIFY(token7.getType() == Token::Type::Ref_op);
    QVERIFY(token7.getOperator() == ast::Operator::Reference);
}

void TokenTest::initColon() {
    Token token8(ast::Operator::Colon);
    QVERIFY(token8.getType() == Token::Type::Colon);
    QVERIFY(token8.getOperator() == ast::Operator::Colon);
}

void TokenTest::initComma() {
    Token token9(ast::Operator::Comma);
    QVERIFY(token9.getType() == Token::Type::Comma);
    QVERIFY(token9.getOperator() == ast::Operator::Comma);
}

void TokenTest::initCondMark() {
    Token token10(ast::Operator::Cond_mark);
    QVERIFY(token10.getType() == Token::Type::Cond_mark);
    QVERIFY(token10.getOperator() == ast::Operator::Cond_mark);
}

void TokenTest::initDot() {
    Token token11(ast::Operator::Dot);
    QVERIFY(token11.getType() == Token::Type::Dot);
    QVERIFY(token11.getOperator() == ast::Operator::Dot);
}

void TokenTest::initLeftBracket() {
    Token token12(ast::Operator::Left_bracket);
    QVERIFY(token12.getType() == Token::Type::L_bracket);
    QVERIFY(token12.getOperator() == ast::Operator::Left_bracket);
}

void TokenTest::initRightBracket() {
    Token token13(ast::Operator::Right_bracket);
    QVERIFY(token13.getType() == Token::Type::R_bracket);
    QVERIFY(token13.getOperator() == ast::Operator::Right_bracket);
}

void TokenTest::initSqLeftBracket() {
    Token token14(ast::Operator::Left_sq_bracket);
    QVERIFY(token14.getType() == Token::Type::Sq_L_bracket);
    QVERIFY(token14.getOperator() == ast::Operator::Left_sq_bracket);
}

void TokenTest::initSqRightBracket() {
    Token token15(ast::Operator::Right_sq_bracket);
    QVERIFY(token15.getType() == Token::Type::Sq_R_bracket);
    QVERIFY(token15.getOperator() == ast::Operator::Right_sq_bracket);
}

void TokenTest::initString()
{
    Token token(std::string("test"));
    QVERIFY(token.getType() == Token::Type::Ident);
    QVERIFY(token.getIdentifier() == "test");
    Token token2("test2");
    QVERIFY(token2.getType() == Token::Type::Ident);
    QVERIFY(token2.getIdentifier() == "test2");
}

void TokenTest::initType()
{
    Token token(ast::ValueType::Int8);
    QVERIFY(token.getType() == Token::Type::Type_int);
    QVERIFY(token.getValueType() == ast::ValueType::Int8);
    Token token2(ast::ValueType::String);
    QVERIFY(token2.getType() == Token::Type::Type_string);
    QVERIFY(token2.getValueType() == ast::ValueType::String);
}
