#ifndef BOOLVALUETEST_H
#define BOOLVALUETEST_H

#include <QObject>
#include <QtTest/QtTest>

#include "../lib/ast/BoolValue.h"

class BoolValueTest: public QObject
{
    Q_OBJECT
public:
    BoolValueTest();

private slots:
    void returnsGivenValue();
    void printProperlyString();
};

#endif // BOOLVALUETEST_H
