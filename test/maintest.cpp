#include <QtTest/QtTest>

#include "TokenTest.h"
#include "ProxyBufferTest.h"
#include "ScannerTest.h"
#include "IntValueTest.h"
#include "BinaryExpressionTest.h"
#include "BoolValueTest.h"
#include "RelationalExpressionTest.h"
#include "LogicBinaryExpressionTest.h"
#include "ExpressionParsingTest.h"
#include "LogicExpressionParsingTest.h"
#include "SymbolTableTest.h"
#include "ParserTest.h"

int main()
{
    TokenTest token;
    QTest::qExec(&token);

    ProxyBufferTest proxy_buffer;
    QTest::qExec(&proxy_buffer);

    ScannerTest scanner;
    QTest::qExec(&scanner);

    IntValueTest int_val;
    QTest::qExec(&int_val);

    BinaryExpressionTest binary_expr;
    QTest::qExec(&binary_expr);

    BoolValueTest bool_val;
    QTest::qExec(&bool_val);

    RelationalExpressionTest rel_expr;
    QTest::qExec(&rel_expr);

    LogicBinaryExpressionTest logic_binary_expr;
    QTest::qExec(&logic_binary_expr);

    ExpressionParsingTest expr_parsing_test;
    QTest::qExec(&expr_parsing_test);

    LogicExpressionParsingTest log_expr_parsing_test;
    QTest::qExec(&log_expr_parsing_test);

    SymbolTableTest sym_table_test;
    QTest::qExec(&sym_table_test);

    ParserTest parser_test;
    QTest::qExec(&parser_test);
    return 0;
}
