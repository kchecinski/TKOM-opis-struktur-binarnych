#ifndef PARSINGTEST_H
#define PARSINGTEST_H

#include "InputTest.h"

#include "../lib/parser/Parser.h"

using namespace parser;

class ParsingTest: public InputTest
{
    Q_OBJECT
public:
    ParsingTest(){}

protected:
    ProxyBuffer* buffer_;
    Scanner* scanner_;
    Parser* parser_;

private slots:
    void init();
    void cleanup();
};

#endif // PARSINGTEST_H
