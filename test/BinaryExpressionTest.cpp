#include "BinaryExpressionTest.h"

#include <memory>

using namespace ast;

void BinaryExpressionTest::sumTwoIntVals()
{
    ArithmBinaryExpression exp(std::make_unique<IntValue>(5),
                         Operator::Plus,
                         std::make_unique<IntValue>(2));
    QCOMPARE(exp.calculate(), 7);
}

void BinaryExpressionTest::subtTwoIntVals()
{
    ArithmBinaryExpression exp(std::make_unique<IntValue>(5),
                         Operator::Minus,
                         std::make_unique<IntValue>(2));
    QCOMPARE(exp.calculate(), 3);
}

void BinaryExpressionTest::multTwoIntVals()
{
    ArithmBinaryExpression exp(std::make_unique<IntValue>(5),
                         Operator::Multiplication,
                         std::make_unique<IntValue>(2));
    QCOMPARE(exp.calculate(), 10);
}

void BinaryExpressionTest::ceilDivTwoIntVals()
{
    ArithmBinaryExpression exp(std::make_unique<IntValue>(5),
                         Operator::Ceil_division,
                         std::make_unique<IntValue>(2));
    QCOMPARE(exp.calculate(), 3);
}

void BinaryExpressionTest::floorDivTwoIntVals()
{
    ArithmBinaryExpression exp(std::make_unique<IntValue>(5),
                         Operator::Floor_division,
                         std::make_unique<IntValue>(2));
    QCOMPARE(exp.calculate(), 2);
}

void BinaryExpressionTest::moduloDivTwoIntVals()
{
    ArithmBinaryExpression exp(std::make_unique<IntValue>(5),
                         Operator::Modulo_division,
                         std::make_unique<IntValue>(2));
    QCOMPARE(exp.calculate(), 1);
}



void BinaryExpressionTest::sumIntValAndBinaryExp()
{
    std::unique_ptr<ArithmBinaryExpression> exp =
            std::make_unique<ArithmBinaryExpression>(std::make_unique<IntValue>(5),
                                               Operator::Plus,
                                               std::make_unique<IntValue>(4));
    ArithmBinaryExpression exp2(std::move(exp),
                          Operator::Plus,
                          std::make_unique<IntValue>(6));
    QCOMPARE(exp2.calculate(), 15);
}

void BinaryExpressionTest::sumTwoBinaryExp()
{
    std::unique_ptr<ArithmBinaryExpression> exp_a =
            std::make_unique<ArithmBinaryExpression>(std::make_unique<IntValue>(1),
                                               Operator::Plus,
                                               std::make_unique<IntValue>(2));
    std::unique_ptr<ArithmBinaryExpression> exp_b =
            std::make_unique<ArithmBinaryExpression>(std::make_unique<IntValue>(3),
                                               Operator::Plus,
                                               std::make_unique<IntValue>(6));
    std::unique_ptr<ArithmBinaryExpression> exp =
            std::make_unique<ArithmBinaryExpression>(std::move(exp_a),
                                               Operator::Plus,
                                               std::move(exp_b));
    QCOMPARE(exp->calculate(), 12);
}

void BinaryExpressionTest::ifNonArithmOperatorThrowException()
{
    std::unique_ptr<ArithmBinaryExpression> exp =
            std::make_unique<ArithmBinaryExpression>(std::make_unique<IntValue>(5),
                                               Operator::Left_bracket,
                                               std::make_unique<IntValue>(4));
    QVERIFY_EXCEPTION_THROWN(exp->calculate(), std::runtime_error);
}


