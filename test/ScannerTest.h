#ifndef SCANNERTEST_H
#define SCANNERTEST_H

#include <QObject>
#include <QtTest/QtTest>

#include <sstream>
#include <string>
#include <list>

#include "../lib/parser/Scanner.h"

using namespace parser;

class ScannerTest : public QObject
{
    Q_OBJECT
public:
    ScannerTest(){}

private:
    std::stringstream in_;
    ProxyBuffer* buffer_;
    Scanner* scanner_;

    void setInput(const std::string &input);

private slots:
    void init();
    void cleanup();

    void scanEmpty();

    void scanDot();
    void scanColon();
    void scanComma();
    void scanLeftBracket();
    void scanRightBracket();
    void scanCondMark();
    void scanGreater();
    void scanGreaterEqual();
    void scanLess();
    void scanLessEqual();
    void scanNotEqual();
    void scanEqual();
    void scanDisjunction();
    void scanConjunction();
    void scanPlus();
    void scanMinus();
    void scanMultiplication();
    void scanFloorDivision();
    void scanCeilDivision();
    void scanModulo();
    void scanNegation();
    void scanReference();
    void scanLeftSqBracket();
    void scanRightSqBracket();

    void scanBadOperators();

    void scanGoodValue();
    void scanTooBigValue();

    void scanIntKeyword();
    void scanStrKeyword();

    void scanIdentifier();
    void scanTooLongIdentifier();
};

#endif // SCANNERTEST_H
