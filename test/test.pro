QT       += testlib

QT       -= gui

TARGET = maintest
CONFIG   += c++14
CONFIG   += console
CONFIG   -= lib_bundle

TEMPLATE = app

SOURCES += maintest.cpp \
    BinaryExpressionTest.cpp \
    IntValueTest.cpp \
    ProxyBufferTest.cpp \
    ScannerTest.cpp \
    TokenTest.cpp \
    BoolValueTest.cpp \
    RelationalExpressionTest.cpp \
    LogicBinaryExpressionTest.cpp \
    ExpressionParsingTest.cpp \
    LogicExpressionParsingTest.cpp \
    ParsingTest.cpp \
    SymbolTableTest.cpp \
    ParserTest.cpp \
    InputTest.cpp

HEADERS += \
    BinaryExpressionTest.h \
    IntValueTest.h \
    ProxyBufferTest.h \
    ScannerTest.h \
    TokenTest.h \
    BoolValueTest.h \
    RelationalExpressionTest.h \
    LogicBinaryExpressionTest.h \
    ExpressionParsingTest.h \
    LogicExpressionParsingTest.h \
    ParsingTest.h \
    SymbolTableTest.h \
    ParserTest.h \
    InputTest.h


DEFINES += SRCDIR=\\\"$$PWD/\\\"

unix:!macx: LIBS += -L$$OUT_PWD/../lib/ -llib

INCLUDEPATH += $$PWD/../lib
DEPENDPATH += $$PWD/../lib

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../lib/liblib.a
