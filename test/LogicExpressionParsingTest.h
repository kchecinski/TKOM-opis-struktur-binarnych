#ifndef LOGICEXPRESSIONPARSINGTEST_H
#define LOGICEXPRESSIONPARSINGTEST_H

#include <QObject>
#include <QtTest/QtTest>

#include "ParsingTest.h"

class LogicExpressionParsingTest: public ParsingTest
{
    Q_OBJECT
public:
    LogicExpressionParsingTest(){}

private slots:

    void parseLogicLiteralDirectly();
    void parseLogicLiteral();
    void parseConjunctionOfTrueAndFalseDirectly();
    void parseConjunctionOfTrueAndTrueDirectly();
    void parseConjunctionOfTrueAndFalse();
    void parseConjunctionOfTrueAndTrue();
    void parseConjunctionOfBoolValueAndRelationalExpression();
    void parseConjunctionOfTwoRelationalExpressions();
    void parseAlternativeOfTrueAndFalse();
    void parseAlternativeOfFalseAndFalse();
    void parseAlternativeOfBoolValueAndConjunction();
    void parseAlternativeOfTwoConjunctions();
    void parseConjunctionOfAlternativeAndBoolValue();
    void parseConjunctionOfTwoTrueAlternatives();
    void parseConjunctionOfTrueAndFalseAlternatives();
};

#endif // LOGICEXPRESSIONPARSINGTEST_H
