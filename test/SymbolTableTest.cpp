#include "SymbolTableTest.h"

void SymbolTableTest::registerInt()
{
    SymbolTable table;
    int val = table.registerInt("ident", 10);
    QCOMPARE(val, 10);
}

void SymbolTableTest::getRegisteredInt()
{
    SymbolTable table;
    table.registerInt("ident", 10);
    int val = table.getIntRef("ident");
    QCOMPARE(val, 10);
}

void SymbolTableTest::changeRegisteredIntValue()
{
    SymbolTable table;
    int& val_ref = table.registerInt("ident", 10);
    val_ref = 15;
    QCOMPARE(table.getIntRef("ident"), 15);
}

void SymbolTableTest::getUnregisteredInt()
{
    SymbolTable table;
    QVERIFY_EXCEPTION_THROWN(table.getIntRef("abc"), std::runtime_error);
}

void SymbolTableTest::getIntRegisteredInParent()
{
    std::shared_ptr<SymbolTable> parent(new SymbolTable);
    SymbolTable child(parent);

    parent->registerInt("a", 10);
    QCOMPARE(child.getIntRef("a"), 10);
}

void SymbolTableTest::getIntUnregisteredInParent()
{
    std::shared_ptr<SymbolTable> parent(new SymbolTable);
    SymbolTable child(parent);

    QVERIFY_EXCEPTION_THROWN(child.getIntRef("abc"), std::runtime_error);
}

void SymbolTableTest::ifIntRegisteredInParentAndChildGetReferenceFromChild()
{
    std::shared_ptr<SymbolTable> parent(new SymbolTable);
    SymbolTable child(parent);

    parent->registerInt("a", 10);
    QCOMPARE(child.getIntRef("a"), 10);

    child.registerInt("a", 15);
    QCOMPARE(child.getIntRef("a"), 15);
}
